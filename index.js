const bodyParser = require('body-parser')
const express = require('express')
const session = require('express-session')
const FileStore = require('session-file-store')
const bcrypt = require('bcryptjs')
const pug = require('pug')
const Sequelize = require('sequelize')
const models = require('./models')
const path = require('path')
const { getProjectInfo, getHelpDeskReport, getIndex, getAmFam, getBillingData } = require('./controllers/web')

const app = express()

//setup pug to render views
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

//Set Session
app.use(session({
    store: new FileStore(session)({ secret: 'secret' }),
    secret: 'secret', //should use random string generator
    resave: true,
    saveUninitialized: false,
    cookie: {
        domain: process.env.COOKIE_DOMAIN, maxAge: process.env.SESS_LIFETIME, httpOnly: true,
        sameSite: true,
    },
}))
app.use(express.static(path.join(__dirname, 'style')));
app.use(bodyParser.urlencoded({ extended: true }))
app.get('/', getProjectInfo, getHelpDeskReport)
app.get('/project', getProjectInfo, getHelpDeskReport)
app.get('/issue', getHelpDeskReport)
app.get('/amfam', getAmFam)
app.get('/billingData', getBillingData)
//app.post('/auth', userLogin)

app.get('/materials', async (request, response) => {
    let newMaterials = []
    const materials = await models.Materials.findAll({
        attributes: ['material_id', 'material_name', 'material_description', 'company_id', 'price_id', 'unit_of_measurement_id'],
        include: [{
            model: models.Companies
        },
        { model: models.PriceDetail },
        { model: models.UOM }]
    })
    for (i = 0; i < materials.length; i++) {
        if (materials[i].dataValues.price_detail === null) {
            newMaterials.push({ id: materials[i].dataValues.material_id, materialName: materials[i].dataValues.material_name, materialDescription: materials[i].dataValues.material_description, company: materials[i].dataValues.company.company_name, unitPrice: 'N/A', unitOfMeasurement: materials[i].dataValues.unit_of_measurement.unit_of_measurement })
        } else {
            newMaterials.push({
                id: materials[i].dataValues.material_id, materialName: materials[i].dataValues.material_name, materialDescription: materials[i].dataValues.material_description, company: materials[i].dataValues.company.company_name, unitPrice: materials[i].dataValues.price_detail.unit_price, unitOfMeasurement: materials[i].dataValues.unit_of_measurement.unit_of_measurement
            })
        }
    }
    response.send(newMaterials)
})
app.get('/uom', async (request, response) => {
    const uom = await models.UOM.findAll()
    response.send(uom)
})

app.get('/companies', async (request, response) => {
    const companies = await models.Companies.findAll()
    response.send(companies)
})

app.get('/products', async (request, response) => {
    const products = await models.Products.findAll()
    response.send(products)
})

app.get('/consumption', async (request, response) => {

    const newList = []


    const consumption = await models.Consumption.findAll({
        attributes: ['quantity', 'company_id', 'object_type_id', 'consumption_datetime', 'bill_of_materials_id'],
        include: [{
            model: models.ObjectType
        },
        {
            model: models.Companies
        },
        {
            model: models.billOfMaterials
        }]
    })
    for (i = 0; i < consumption.length; i++) {
        if (consumption[i].dataValues.bill_of_material === null) {
            newList.push({ date: consumption[i].dataValues.consumption_datetime, quantity: consumption[i].dataValues.quantity, company: consumption[i].company.company_name, object_type: consumption[i].object_type.object_type, billOfMaterials: null })
        } else {
            newList.push({
                date: consumption[i].dataValues.consumption_datetime, quantity: consumption[i].dataValues.quantity, company: consumption[i].company.company_name, object_type: consumption[i].object_type.object_type, billOfMaterials: consumption[i].bill_of_material
            })
        }
    }
    response.send(newList)
})
app.get('/billofmaterials', async (request, response) => {
    const billingData = []

    const billOfMaterials = await models.billOfMaterials.findAll({
        attributes: ['bill_of_materials_id', 'product_id', 'company_id', 'object_id', 'object_type_id', 'record_last_changed_date'],
        include: [{
            model: models.Products,
            include: models.JOD
        },
        { model: models.Companies },
        { model: models.ObjectType },
        {
            model: models.Services,
            include: [{
                model: models.PriceMaster,
                include: models.PriceDetail
            }]

        }]
    })
    //for (i = 0; i < billOfMaterials.length; i++){
    //  billingData.push({})
    // }
    response.send(billOfMaterials)
})
app.get('/job_status', async (request, response) => {
    const jobStatus = await models.JobStatus.findAll()
    response.send(jobStatus)
})

app.get('/objecttype', async (request, response) => {
    const objectType = await models.ObjectType.findAll()
    response.send(objectType)
})



app.get('/services', async (request, response) => {
    const services = await models.Services.findAll()
    response.send(services)
})

app.get('/priceDetail', async (request, response) => {
    const priceDetail = await models.PriceDetail.findAll()
    response.send(priceDetail)
})

app.get('/materialhistory', async (request, response) => {
    const materialHistory = await models.MaterialHistory.findAll()
    response.send(materialHistory)
})

app.get('/priceMaster', async (request, response) => {
    const newObject = []
    const priceMaster = await models.PriceMaster.findAll({

        include: models.PriceDetail
    })
    /*   for (i = 0; i < priceMaster.length; i++) {
           newObject.push({ id: priceMaster[i].dataValues.price_id, description: priceMaster[i].dataValues.price_description, status: priceMaster[i].dataValues.active_record, company: priceMaster[i].companies[1] })
       }
       console.log(newObject)*/
    response.send(priceMaster)
})

app.get('/jod', async (request, response) => {
    const jod = await models.JOD.findAll({
        // attributes: ['option_id', 'site_id', 'client_id', 'product_id', 'job_id', 'job_status_id', 'received_datetime', 'mailed_quantity', 'received_quantity'],
        include: [{ model: models.Site },
        { model: models.Companies },
        { model: models.Products },
        { model: models.Jobs },
        { model: models.JobStatus }]
    })
    response.send(jod)
})

app.get('/jobs', async (request, response) => {
    const jobs = await models.Jobs.findAll()
    response.send(jobs)
})

app.get('/sites', async (request, response) => {
    const sites = await models.Site.findAll()
    response.send(sites)
})

const server = app.listen(1337, () => { console.log('Listening on port 1337') })

module.exports = server