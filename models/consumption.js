const consumptionTable = (connection, Sequelize, ObjectType, Companies, billOfMaterials) => {
    return connection.define(
        'consumption',
        {
            consumption_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
            bill_of_materials_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: billOfMaterials, key: 'bill_of_materials_id' } },
            quantity: { type: Sequelize.INTEGER },
            consumption_datetime: { type: Sequelize.DATEONLY },
            object_id: { type: Sequelize.INTEGER },
            object_type_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: ObjectType, key: 'object_type_id' } },
            record_last_changed_by: { type: Sequelize.STRING },
            changed_by_company_id: { type: Sequelize.INTEGER },
            company_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: Companies, key: 'company_id' } }
        }, { freezeTableName: true, timestamps: false, underscored: true }
    )
}

module.exports = consumptionTable