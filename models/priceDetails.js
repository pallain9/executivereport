const PriceDetails = (connection, Sequelize, Companies, PriceMaster) => {
    return connection.define(
        'price_details',
        {
            price_detail_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
            max_quantity: { type: Sequelize.INTEGER },
            price_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: PriceMaster, key: 'price_id' } },
            unit_price: { type: Sequelize.INTEGER },
            unit_postage: { type: Sequelize.INTEGER },
            setup_fee: { type: Sequelize.INTEGER },
            ship_handling_fee: { type: Sequelize.INTEGER },
            active_record: { type: Sequelize.STRING },
            record_last_changed_by: { type: Sequelize.STRING },
            changed_by_company_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: Companies, key: 'company_id' } },
            record_last_changed_date: { type: Sequelize.STRING },
            customer_unit_price: { type: Sequelize.INTEGER },
            postage_class: { type: Sequelize.STRING },
            min_quantity: { type: Sequelize.INTEGER }
        }, { freezeTableName: true, timestamps: false })
}

module.exports = PriceDetails