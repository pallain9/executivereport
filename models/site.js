const Site = (connection, Sequelize) => {
    return connection.define(
        'site',
        {
            site_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
            site_code: { type: Sequelize.INTEGER },
            site_description: { type: Sequelize.STRING },
            record_made_active_date: { type: Sequelize.STRING },
            record_added_date: { type: Sequelize.STRING },
            record_last_changed_by: { type: Sequelize.STRING },
            record_last_changed_date: { type: Sequelize.STRING }
        }, { freezeTableName: true, timestamps: false }
    )
}
module.exports = Site