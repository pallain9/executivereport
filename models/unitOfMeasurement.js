const UOM = (connection, Sequelize) => {
    return connection.define(
        'unit_of_measurement',
        {
            unit_of_measurement_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
            unit_of_measurement: { type: Sequelize.STRING },
        }, { freezeTableName: true, timestamps: false }
    )
}

module.exports = UOM