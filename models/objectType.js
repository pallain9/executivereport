const ObjectType = (connection, Sequelize) => {
    return connection.define(
        'object_type',
        {
            object_type_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
            object_type: { type: Sequelize.STRING },
        }, { freezeTableName: true, timestamps: false }
    )
}

module.exports = ObjectType