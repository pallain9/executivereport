const PriceMaster = (connection, Sequelize, Companies) => {
    return connection.define(
        'price_master',
        {
            price_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
            price_description: { type: Sequelize.STRING },
            active_record: { type: Sequelize.STRING },
            record_last_changed_by: { type: Sequelize.STRING },
            changed_by_company_id: { type: Sequelize.INTEGER },
            record_last_changed_date: { type: Sequelize.STRING },
            company_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: Companies, key: 'company_id' } }
        }, { freezeTableName: true, timestamps: false })
}

module.exports = PriceMaster