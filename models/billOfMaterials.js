const billOfMaterialsTable = (connection, Sequelize, Products, Companies, ObjectType, Services) => {
    return connection.define(
        'bill_of_materials',
        {
            bill_of_materials_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
            product_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: Products, key: 'Product_id' } },
            company_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: Companies, key: 'company_id' } },
            object_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: Services, key: 'service_id' } },
            object_type_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: ObjectType, key: 'object_type_id' } },
            active_record: { type: Sequelize.INTEGER },
            record_last_changed_by: { type: Sequelize.INTEGER },
            changed_by_company_id: { type: Sequelize.INTEGER },
            record_last_changed_date: { type: Sequelize.STRING, }
        }, { freezeTableName: true, timestamps: false }
    )
}

module.exports = billOfMaterialsTable