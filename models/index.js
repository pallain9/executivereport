const Sequelize = require('sequelize')
const UOMModel = require('./unitOfMeasurement')
const CompaniesModel = require('./companies')
const ConsumptionModel = require('./consumption')
const MaterialsModel = require('./materials')
const ProductsModel = require('./products')
const BillOfMaterialsModel = require('./billOfMaterials')
const JobStatusModel = require('./jobStatus')
const ObjectTypeModel = require('./objectType')
const ServiceModel = require('./service')
const priceMasterModel = require('./priceMaster')
const priceDetailModel = require('./priceDetails')
const materialHistoryModel = require('./materialHistory')
const JODModel = require('./jod')
const JobsModel = require('./jobs')
const SiteModel = require('./site')

const connection = new Sequelize('CIS', 'pallain9', 'Brayden9!', {
    host: 'localhost',
    dialect: 'mssql'
})

const UOM = UOMModel(connection, Sequelize)
const Companies = CompaniesModel(connection, Sequelize)
const Consumption = ConsumptionModel(connection, Sequelize)
const Materials = MaterialsModel(connection, Sequelize)
const Products = ProductsModel(connection, Sequelize)
const billOfMaterials = BillOfMaterialsModel(connection, Sequelize)
const ObjectType = ObjectTypeModel(connection, Sequelize)
const JobStatus = JobStatusModel(connection, Sequelize)
const Services = ServiceModel(connection, Sequelize)
const PriceMaster = priceMasterModel(connection, Sequelize)
const PriceDetail = priceDetailModel(connection, Sequelize)
const MaterialHistory = materialHistoryModel(connection, Sequelize)
const JOD = JODModel(connection, Sequelize)
const Jobs = JobsModel(connection, Sequelize)
const Site = SiteModel(connection, Sequelize)

Consumption.belongsTo(ObjectType, { foreignKey: 'object_type_id' })
Consumption.belongsTo(Companies, { foreignKey: 'company_id' })
Consumption.belongsTo(billOfMaterials, { foreignKey: 'bill_of_materials_id' })

PriceMaster.hasMany(Companies, { foreignKey: 'company_id' })
billOfMaterials.belongsTo(Products, { foreignKey: 'product_id' })
billOfMaterials.belongsTo(Companies, { foreignKey: 'company_id' })
billOfMaterials.belongsTo(ObjectType, { foreignKey: 'object_type_id' })
Materials.belongsTo(Companies, { foreignKey: 'company_id' })
Materials.belongsTo(PriceDetail, { foreignKey: 'price_id' })
Materials.belongsTo(UOM, { foreignKey: 'unit_of_measurement_id' })
PriceMaster.belongsTo(Companies, { foreignKey: 'company_id' })
JOD.belongsTo(Site, { foreignKey: 'site_id' })
JOD.belongsTo(Companies, { foreignKey: 'client_id', targetKey: 'company_id' })
JOD.hasMany(Products, { foreignKey: 'product_id' })
JOD.belongsTo(Jobs, { foreignKey: 'job_id' })
JOD.belongsTo(JobStatus, { foreignKey: 'job_status_id' })
Products.belongsTo(JOD, { foreignKey: 'product_id' })
Services.belongsTo(billOfMaterials, { foreignKey: 'service_id', targetKey: 'object_id' })
billOfMaterials.belongsTo(Services, { foreignKey: 'object_id', targetKey: 'service_id' })
Services.belongsTo(PriceMaster, { foreignKey: 'price_id' })
PriceMaster.belongsTo(PriceDetail, { foreignKey: 'price_id' })

module.exports = {
    UOM,
    Companies,
    Consumption,
    Materials,
    Products,
    billOfMaterials,
    ObjectType,
    JobStatus,
    Services,
    PriceMaster,
    PriceDetail,
    MaterialHistory,
    JOD,
    Jobs,
    Site
}