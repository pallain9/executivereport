const Materials = (connection, Sequelize, Companies, unitOfMeasurement, PriceDetails) => {
    return connection.define(
        'materials',
        {
            material_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
            material_type: { type: Sequelize.STRING },
            material_description: { type: Sequelize.STRING },
            company_id: { type: Sequelize.INTEGER, reference: { model: Companies, key: 'company_id' } },
            price_id: { type: Sequelize.INTEGER, reference: { model: PriceDetails, key: 'price_id' } },
            unit_of_measurement_id: { type: Sequelize.INTEGER, reference: { model: unitOfMeasurement, key: 'unit_of_measurement_id' } },
            version_number: { type: Sequelize.INTEGER },
            active_record: { type: Sequelize.INTEGER },
            changed_by_company_id: { type: Sequelize.INTEGER, reference: { model: Companies, key: 'company_id' } },
            record_last_changed_by: { type: Sequelize.INTEGER },
            site_id: { type: Sequelize.INTEGER },
        }, { freezeTableName: true, timestamps: false }
    )
}

module.exports = Materials