const JobStatus = (connection, Sequelize) => {
    return connection.define(
        'job_status',
        {
            job_status_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
            job_status_code: { type: Sequelize.INTEGER },
            job_status_descriptions: { type: Sequelize.STRING },
            active_record: { type: Sequelize.INTEGER },
            record_made_active_date: { type: Sequelize.STRING },
            record_made_inactive_date: { type: Sequelize.STRING },
            record_added_date: { type: Sequelize.STRING },
            record_last_changed_by: { type: Sequelize.STRING },
            record_last_changed_date: { type: Sequelize.STRING },
            last_reviewed_by: { type: Sequelize.STRING },
            last_reviewed_date: { type: Sequelize.STRING },
            times_accessed: { type: Sequelize.STRING },
            last_accessed_date: { type: Sequelize.STRING },
            status_type: { type: Sequelize.STRING },
            color: { type: Sequelize.STRING },
            is_comment_required: { type: Sequelize.STRING },
            close_workflow: { type: Sequelize.STRING },
            status_action_type: { type: Sequelize.STRING }
        }, { freezeTableName: true, timestamps: false }
    )
}

module.exports = JobStatus