
const Services = (connection, Sequelize, Companies, UOM, PriceMaster) => {
    return connection.define(
        'services',
        {
            service_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
            service_name: { type: Sequelize.STRING },
            service_description: { type: Sequelize.STRING },
            service_frequency: { type: Sequelize.STRING },
            company_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: Companies, key: 'company_id' } },
            unit_of_measurement_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: UOM, key: 'unit_of_measurement_id' } },
            price_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: PriceMaster, key: 'price_id' } },
            active_record: { type: Sequelize.STRING },
            record_last_changed_by: { type: Sequelize.STRING },
            changed_by_company_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: Companies, key: 'company_id' } },
            record_last_changed_date: { type: Sequelize.STRING }
        }, { freezeTableName: true, timestamps: false })
}

module.exports = Services