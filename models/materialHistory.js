const MaterialHistory = (connection, Sequelize, billOfMaterials, Companies, Materials) => {
    return connection.define(
        'material_history',
        {
            material_history_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
            company_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: Companies, key: 'company_id' } },
            material_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: Materials, key: 'material_id' } },
            previous_quantity: { type: Sequelize.INTEGER },
            transaction_quantity: { type: Sequelize.INTEGER },
            current_quantity: { type: Sequelize.INTEGER },
            description: { type: Sequelize.STRING },
            profile_id: { type: Sequelize.INTEGER },
            transaction_date: { type: Sequelize.INTEGER },
            bill_of_materials_id: { type: Sequelize.INTEGER, primaryKey: true, reference: { model: billOfMaterials, key: 'bill_of_materials_id' } }

        }, { freezeTableName: true, timestamps: false }
    )
}
module.exports = MaterialHistory