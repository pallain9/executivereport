'use strict';
const axios = require('axios')
const models = require('../models')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
async function getIndex(request, response) {
    return response.render('index')
}

async function getProjectInfo(request, response) {
    let i
    let updatedProject = []
    let project = await axios.get(`https://massprinting.atlassian.net/rest/api/3/project`,
        {
            auth: {
                username: 'pallain@massprinting.com',
                password: '53pEsbRGt9cG7h4m3DX5CDDF'
            }
        })

    for (i = 0; i < project.data.length; i++) {
        let category = project.data[i].projectCategory


        if (category === undefined) {
            updatedProject.push({ name: project.data[i].name, id: project.data[i].id, key: project.data[i].key, Category: 'N/A', })
        } else {
            updatedProject.push({ name: project.data[i].name, id: project.data[i].id, key: project.data[i].key, Category: category.name })
        }
    }
    return updatedProject
        ? response.render('index', { updatedProject })
        : response.sendStatus(404)

}


async function getHelpDeskReport(request, response) {
    let i
    let issue = await axios.get(`https://massprinting.atlassian.net/rest/api/3/search?jql=project=SD2&status!=Resolved&maxResults=100`,
        {
            auth: {
                username: 'pallain@massprinting.com',
                password: '53pEsbRGt9cG7h4m3DX5CDDF'
            },
            where: {}

        })

    let selectedData = []

    for (i = 0; i < issue.data.issues.length; i++) {
        //let ticketCount = issue.data.issues.length
        //console.log(ticketCount)

        if (issue.data.issues[i].fields.status.name !== 'Resolved')
            selectedData.push({ id: issue.data.issues[i].id, created: issue.data.issues[i].fields.created, key: issue.data.issues[i].key, status: issue.data.issues[i].fields.status.name })
    }

    return selectedData
        ? response.render('issue', { selectedData })
        : response.sendStatus(404)

}

async function getAmFam(request, response) {
    let i
    let amfam = await axios.get(`https://massprinting.atlassian.net/rest/api/3/search?jql=project=EA`,
        {
            auth: {
                username: 'pallain@massprinting.com',
                password: '53pEsbRGt9cG7h4m3DX5CDDF'
            }


        })

    let selectedData = []
    for (i = 0; i < amfam.data.issues.length; i++) {
        selectedData.push({ id: amfam.data.issues[i].id, project: amfam.data.issues[i].fields.project, time: amfam.data.issues[i].fields.timespent })
    }
    console.log(selectedData)
    return selectedData
        ? response.render('amfam', { selectedData })
        : response.sendStatus(404)

}

async function getBillingData(request, response) {
    let i = 0
    const billingReport = []
    const processingData = await models.Consumption.findAll({
        limit: 1000,
        order: [['consumption_datetime', 'DESC']],
        include: [{
            model: models.billOfMaterials,
            include: [{
                model: models.Products,
                include: models.JOD
            },
            { model: models.Companies },
            { model: models.ObjectType },
            {
                model: models.Services,
                include: [{
                    model: models.PriceMaster,
                    include: models.PriceDetail
                }]

            }]
        }]
    })
    for (i = 0; i < processingData.length; i++) {
        if (processingData[i].dataValues.bill_of_material === null || processingData[i].dataValues.bill_of_material.Product === null || processingData[i].dataValues.bill_of_material.service === null || processingData[i].dataValues.bill_of_material.service.price_master === null || processingData[i].dataValues.bill_of_material.object_type === null || processingData[i].dataValues.bill_of_material.service.price_master.price_detail === null) {
            billingReport.push({
                consumption_id: processingData[i].dataValues.consumption_id, date: processingData[i].dataValues.consumption_datetime, quantity: processingData[i].dataValues.quantity,
                status: 'NULL'
            })
        } else {
            billingReport.push({
                consumption_id: processingData[i].dataValues.consumption_id, date: processingData[i].dataValues.consumption_datetime, quantity: processingData[i].dataValues.quantity, bom_id: processingData[i].dataValues.bill_of_material.bill_of_materials_id,
                product_id: processingData[i].dataValues.bill_of_material.Product.product_id, product: processingData[i].dataValues.bill_of_material.Product.product_num,
                company_id: processingData[i].dataValues.bill_of_material.company.company_id, client_name: processingData[i].dataValues.bill_of_material.company.company_name,
                object_type_id: processingData[i].dataValues.bill_of_material.object_type.object_type_id, object_type: processingData[i].dataValues.bill_of_material.object_type.object_type,
                service_id: processingData[i].dataValues.bill_of_material.service.service_id, service_name: processingData[i].dataValues.bill_of_material.service.service_name, service_description: processingData[i].dataValues.bill_of_material.service.service_description,
                costPrice: processingData[i].dataValues.bill_of_material.service.price_master.price_detail.unit_price,
                sellPrice: processingData[i].dataValues.bill_of_material.service.price_master.price_detail.customer_unit_price
            })
        }
    }
    response.send(billingReport)
}

function sumQuantity(transaction) {
    let newList = transaction.filter((quantity) => { return quantity.client_name === 'Leatherstocking' && quantity.service_name.includes('1st Page') })
    let sum = newList.reduce(function (accumulator, currentValue) {
        return accumulator + currentValue.quantity
    }, 0)

    return sum
}


module.exports = { sumQuantity, getProjectInfo, getHelpDeskReport, getIndex, getAmFam, getBillingData, }