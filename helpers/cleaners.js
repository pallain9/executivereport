const newProject = (projectTable) => {
    return {
        name: projectTable.name,
        id: projectTable.id,
        key: projectTable.key,
        projectCategory: projectTable.projectCategory
    }
}

module.exports = {
    newProject
}